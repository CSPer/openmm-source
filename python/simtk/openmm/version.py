
# THIS FILE IS GENERATED FROM OPENMM SETUP.PY
short_version = '7.0.1'
version = '7.0.1'
full_version = '7.0.1.dev-5e86c4f'
git_revision = '5e86c4f76cb8e40e026cc78cdc452cc378151705'
release = False
openmm_library_path = r'/home/vagrant/install/lib'

if not release:
    version = full_version
