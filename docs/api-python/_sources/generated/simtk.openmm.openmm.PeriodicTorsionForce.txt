PeriodicTorsionForce
========================================

.. currentmodule:: simtk.openmm.openmm

.. autoclass:: PeriodicTorsionForce

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~PeriodicTorsionForce.__init__
      ~PeriodicTorsionForce.addTorsion
      ~PeriodicTorsionForce.getForceGroup
      ~PeriodicTorsionForce.getNumTorsions
      ~PeriodicTorsionForce.getTorsionParameters
      ~PeriodicTorsionForce.setForceGroup
      ~PeriodicTorsionForce.setTorsionParameters
      ~PeriodicTorsionForce.updateParametersInContext
      ~PeriodicTorsionForce.usesPeriodicBoundaryConditions
   
   

   
   
   