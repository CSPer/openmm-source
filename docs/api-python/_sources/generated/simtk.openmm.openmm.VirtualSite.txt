VirtualSite
===============================

.. currentmodule:: simtk.openmm.openmm

.. autoclass:: VirtualSite

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~VirtualSite.__init__
      ~VirtualSite.getNumParticles
      ~VirtualSite.getParticle
   
   

   
   
   